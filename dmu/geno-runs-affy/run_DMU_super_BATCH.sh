#!/bin/bash
#SBATCH -n 1            	# 1 core
# Job name:
#SBATCH --job-name=DMU_super # job name
#
# Project:
#SBATCH --account=nn4653k
#
# Wall clock limit:
#SBATCH --time=05:00:00
#
# Max memory usage:
#SBATCH --mem-per-cpu=4G
python DMU_super.py
