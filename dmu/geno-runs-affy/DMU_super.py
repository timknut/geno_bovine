"""
Runs DMU on polaris, using a specified number of processors. 


"""
import util
import os
import sys
import shutil

#-----------------------------------------------------------------

NTHREADS = 100 #number of processors to use
genofilename = '/usit/abel/u1/tikn/geno_bovine/dmu/DMU-data/final_777genos'	#affy format, i.e. a one-letter code for each genotype (0, 1, 2, or -1), one header line, first genotype in column 2. 
#phenofilename = '/usit/abel/u1/tikn/geno_bovine/dmu/DMU-data/final_dmudata.txt'
phenofilename = '/usit/abel/u1/tikn/dmu/geno-runs-affy/777_dmudata2-red.txt'
tempdir = '/usit/abel/u1/tikn/geno_bovine/dmu/geno-runs-affy/temp/'	#folder where all temp files are put
sbatchtemplatefile = file('/usit/abel/u1/tikn/geno_bovine/dmu/run_melk/template2.job', 'r') #template for submitting job scripts
no_of_effects_H1 = 3	#the number of effects in the full DMU model
outfolder = '/usit/abel/u1/tikn/dmu/geno-runs-affy/out/'	#the folder where the (final) output files are to be kept
templatefileH0name = '/usit/abel/u1/tikn/dmu/geno-runs-affy/storfe_H0_2.DIR'
templatefileH1name = '/usit/abel/u1/tikn/dmu/geno-runs-affy/storfe_H1_2.DIR'
missingindicator = '-1'	#MISSING INDICATOR USED IN GENOTYPE FILE! (can use other missing indicator in phenotype file). 
mainDMUprogram = '/usit/abel/u1/tikn/geno_bovine/dmu/run_melk/DMU_main.py'
#-----------------------------------------------------------------


genofile = file(genofilename, 'r')	#affy format genotype file
phenofile = file(phenofilename, 'r')	#phenotype file, same animals as in genofile (and in the same order). 

#test that genofile and phenofile contain the same markers, in the same order (just a safety measure, can be removed): 
genofile.readline()
for line in genofile: 
	line2 = phenofile.readline()
	if line.strip().split('\t')[0] != line2.strip().split('\t')[0]:
		print 'genofile and phenofile do not contain the same animals, in the same order.'
genofile.seek(0)
phenofile.seek(0)		

#make NTHREADS smaller geno-files: 
lheader = genofile.readline().strip().split('\t') 
nmarkers = len(lheader)-1
nmarkersperfile = int(nmarkers/NTHREADS)


for currthread in range(NTHREADS): 

	#make working directories from which to run 
	rundir = tempdir + 'rundirs/' + str(currthread) + '/'
	if not os.path.exists(rundir):
		os.makedirs(rundir)
	
	outfilename = tempdir + 'genos' + str(currthread)
	outfile = file(outfilename, 'w')
	
	#write header:
	if currthread == NTHREADS-1:	#for last thread only; makes sure that all snps are written to file
		outfile.write('#' + '\t' + '\t'.join(lheader[(currthread*nmarkersperfile)+1:]) + '\n')
	else:
		outfile.write('#' + '\t' + '\t'.join(lheader[(currthread*nmarkersperfile)+1:((currthread+1)*nmarkersperfile)+1]) + '\n')
	
	#write rest of file:
	genofile.seek(0)	#need to go back because genofile is read multiple times
	genofile.readline()	#skip header
	for line in genofile: 
		llin = line.strip().split('\t')
		if currthread == NTHREADS-1:	#last thread
			outfile.write(llin[0] + '\t' + '\t'.join(llin[(currthread*nmarkersperfile+1):]) + '\n')
		else:
			outfile.write(llin[0] + '\t' + '\t'.join(llin[(currthread*nmarkersperfile+1):((currthread+1)*nmarkersperfile)+1]) + '\n')
	
	#transpose file: 
	outfile.close()
	util.transpose_file_newest(outfilename, outfilename + '.transposed')	#transpose the file so that the markers appear in rows (faster to read downstream)
	os.remove(outfilename)	#remove the non-transposed file
	
	#make job script and submit it::
	sbatchfile = file(tempdir + 'run' + str(currthread) + '.job', 'w')
	sbatchtemplatefile.seek(0)
	sbatchfile.write(sbatchtemplatefile.read())
	sbatchfile.write('\nmodule load python2')
	sbatchfile.write('\ncd ' + rundir)

	#endre:
	sbatchfile.write('\npython ' + mainDMUprogram + ' ' + ' '.join([str(currthread), outfilename + '.transposed', phenofilename, tempdir, str(no_of_effects_H1), outfolder, templatefileH0name, templatefileH1name, missingindicator]))
	#..........

	sbatchfile.close()
	#endre:
	command = 'sbatch ' + tempdir + 'run' + str(currthread) + '.job'	#command calling on sbatch to run the analysis for the SNPs in that file
	#............................
	os.system(command)
	
	
	
genofile.close()
phenofile.close()
	
#check that all jobs have terminated, then pull together the output files and delete the geno files that were made by this script. 
